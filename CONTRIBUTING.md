import redis
import json
from pprint import pprint

r = redis.StrictRedis(host='127.0.0.1', port=6379, db = 0)

# Introducir 6 palabras y sus significados
r.set('Xopa','Que paso bro')
r.mset({'Mopri':'Amigo','Chuzo':'Sorpresa'})
r.mset({'Cocoa':'Chisme','Pelaita':'Chiquilla','Vaina':'Cualquier cosa'})

# Introducir 6 palabras y sus significados
r.set('Pipa','Coco')
r.mset({'Ofi':'Si','Chuleta':'Queja'})
r.mset({'Cool':'Genial','Yumbo':'Grande','Chombo':'Moreno'})
print("")


# Imprimir toda la base de datos
print("Xopa",r.get('Xopa'))
print("Mopri",r.get('Mopri'))
print("Chuzo",r.get('Chuzo'))
print("Cocoa",r.get('Cocoa'))
print("Pelaita",r.get('Pelaita'))
print("Vaina",r.get('Vaina'))
print("Pipa",r.get('Pipa'))
print("Ofi",r.get('Ofi'))
print("Chuleta",r.get('Chuleta'))
print("Cool",r.get('Cool'))
print("Yumbo",r.get('Yumbo'))
print("Chombo",r.get('Chombo'))


print("")
print("Xopa",r.get('Xopa'))
print("Mopri",r.get('Mopri'))
print("")
print("Chuzo,Cocoa,Pelaita",r.mget({'Chuzo','Cocoa','Pelaita'}))
print("")
print("Vaina","Pipa","Ofi",r.mget({'Vaina','Pipa','Ofi'}))
print("")
print("Yumbo",r.get('Yumbo'))
print("Chombo",r.get('Chombo'))

# Borramos los numeros impares 6 datos
print(r.delete('Xopa'))
print(r.delete('Chuzo'))
print(r.delete('Pelaita'))
print(r.delete('Pipa'))
print(r.delete('Chuleta'))
print(r.delete('Yumbo'))

print("")

# Imprimir toda la base de datos
print("Xopa",r.get('Xopa'))
print("Mopri",r.get('Mopri'))
print("Chuzo",r.get('Chuzo'))
print("Cocoa",r.get('Cocoa'))
print("Pelaita",r.get('Pelaita'))
print("Vaina",r.get('Vaina'))
print("Pipa",r.get('Pipa'))
print("Ofi",r.get('Ofi'))
print("Chuleta",r.get('Chuleta'))
print("Cool",r.get('Cool'))
print("Yumbo",r.get('Yumbo'))
print("Chombo",r.get('Chombo'))

# Insertar 6 nuevos datos con sus significados para sustituir los 6 que se borraron introduciendolos con r.mset de 3 en 3
# A continuacion se hace un print de espacio y se mandan a imprimir con get de 2 en 2
r.mset({'Llezca':'Calle','Chambon':'Persona sin habilidades','Birria':'Juego'})
r.mset({'Chachai':'Ropa bonita','Chainiao':'Persona bien vestida','Tongo':'Policia'})
print(r.mget('Llezca','Chambon'))
print(r.mget('Birria','Chachai'))
print(r.mget('Chainiao','Tongo'))

